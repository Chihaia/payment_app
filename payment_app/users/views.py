from django.http import HttpResponse
from django.contrib.auth import login, authenticate
from django.contrib.sites.shortcuts import get_current_site
from django.utils.encoding import force_bytes, force_text
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.template.loader import render_to_string
from .tokens import account_activation_token
from django.shortcuts import render, redirect
from django.contrib import messages
from .forms import UserRegisterForm, UserUpdateForm, UserAdminCreationForm, UserAdminChangeForm
from django.contrib.auth.decorators import login_required
from .models import User
from django.core.mail import EmailMessage


#@user_passes_test(User, login_url='profile')
def register(request):
    if request.method == 'POST':
        form = UserRegisterForm(request.POST)
        if form.is_valid():
            user = form.save(commit=False)
            user.is_active = False
            user.save()
            form.save()
            current_site = get_current_site(request)
            mail_subject = 'Activeaza contul tau'
            message = render_to_string('users/acc_active_email.html', {
                'user': user,
                'domain': current_site.domain,
                'uid':urlsafe_base64_encode(force_bytes(user.pk)),
                'token':account_activation_token.make_token(user),
            })
            email = form.cleaned_data.get('email')
            email = EmailMessage(
                        mail_subject, message, to=[email]
            )
            email.send()
            messages.success(request, f'Contul a fost creat, verificati adresa de email pentru activare')
            return redirect('login')
            #return redirect('login')
    else:
        form = UserRegisterForm()
    return render(request, 'users/register.html', {'form': form, 'title': 'Register'})


@login_required
def home(request):
    return render(request, 'users/home.html', {'title': 'Dashboard'})

@login_required
# Update Profile Page
def profile_update(request):
    return render(request, 'users/profile_update.html')

# Modify the user details when is authenticated
@login_required
def profile(request):
    if request.method == 'POST':
        u_form = UserUpdateForm(request.POST, instance=request.user)
        if u_form.is_valid():
            u_form.save()
            messages.success(request, f'Informatiile de profil au fost actualizate, accesati link-ul Profile pentru vizualizare')
            return redirect('profile_update')
    else:
        u_form = UserUpdateForm(instance=request.user)
    context = {
        'u_form' : u_form,
        'title' : 'Profile'
    }

    return render(request, 'users/profile.html', context)


# Activate account via email
def activate(request, uidb64, token):
    try:
        uid = force_text(urlsafe_base64_decode(uidb64))
        user = User.objects.get(pk=uid)
    except(TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None
    if user is not None and account_activation_token.check_token(user, token):
        user.is_active = True
        user.save()
        messages.success(request, f'Contul de e-mail a fost confirmat, va rugam sa va autentificati mai jos cu adresa de mail si parola')
        return redirect('login')
    else:
        return HttpResponse('Activation link is invalid!')