from django.db import models
from django.contrib.auth.models import (
    AbstractBaseUser, BaseUserManager
)


class UserManager(BaseUserManager):
    def create_user(self, email, nume, prenume, telefon, password=None, is_active=True, is_staff=False, is_admin=False):
        if not email:
            raise ValueError("Users must have an email address")
        if not password:
            raise ValueError("Does not have a password")
        if not nume and prenume:
            raise ValueError("The user need to have a name")
        if not telefon:
            raise ValueError("The user don't have phone number")
        user_obj = self.model(
            email=self.normalize_email(email)
        )
        user_obj.set_password(password)
        user_obj.nume = nume
        user_obj.prenume = prenume
        user_obj.telefon = telefon
        user_obj.staff = is_staff
        user_obj.admin = is_admin
        user_obj.is_active = is_active
        user_obj.save(using=self._db)
        return user_obj

    def create_staffuser(self, email, nume, prenume, telefon, password=None):
        user = self.create_user(
            email,
            nume=nume,
            prenume=prenume,
            telefon=telefon,
            password=password,
            is_staff=True
        )
        return user

    def create_superuser(self, email, nume, prenume, telefon, password=None):
        user = self.create_user(
            email,
            nume=nume,
            prenume=prenume,
            telefon=telefon,
            password=password,
            is_staff=True,
            is_admin=True,
        )
        return user


class User(AbstractBaseUser):
    email = models.EmailField(max_length=255, unique=True)
    nume = models.CharField(max_length=100, blank=False, null=False)
    prenume = models.CharField(max_length=100, blank=False, null=False)
    telefon = models.CharField(max_length=11, blank=False, null=False)
    is_active = models.BooleanField(default=True)  # can login
    staff = models.BooleanField(default=False)  # non superuser
    admin = models.BooleanField(default=False)  # superuser
    timestamp = models.DateTimeField(auto_now=True)
    # confirm = models.BooleanFiled(default=False)
    # confirmed_date = models.DateTimeField(default=False)

    USERNAME_FIELD = 'email'  # username
    # email and password are required by default
    REQUIRED_FIELDS = ['nume', 'prenume', 'telefon']

    objects = UserManager()

    def __str__(self):
        return self.email

    def get_name(self):
        return self.nume

    def get_surname(self):
        return self.prenume

    def get_phone(self):
        return self.telefon

    def get_date(self):
        return self.timestamp

    def has_perm(self, perm, obj=None):
        return True

    def has_module_perms(self, app_label):
        return True

    @property
    def is_staff(self):
        return self.staff

    def is_admin(self):
        return self.admin



class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    # extend extra data
    def __str__(self):
        return f'{self.user.email} Profile'

