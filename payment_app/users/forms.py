from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.models import User
from django.contrib.auth.forms import ReadOnlyPasswordHashField
from .models import User
from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _

"""
# Name validator
def validate_name(value):
    val = value.split()
    if len(val) < 2:
        raise ValidationError(
            _('%(value)s nu reprezinta numele complet'),
            params={'value': value},
        )
"""

class CustomAuthForm(AuthenticationForm):
    def __init__(self, *args, **kwargs):
        self.error_messages['invalid_login'] = 'Custom error'
        super().__init__(*args, **kwargs)


class UserRegisterForm(UserCreationForm):
    email = forms.EmailField(widget=forms.TextInput(attrs={"placeholder":"exemplu@email.com"}))
    nume = forms.CharField(widget=forms.TextInput(attrs={"placeholder":"Numele dumneavoastra"}))
    prenume = forms.CharField(widget=forms.TextInput(attrs={"placeholder":"Prenumele dumneavoastra"}))
    telefon = forms.CharField(widget=forms.TextInput(attrs={"placeholder":"Numar de telefon"}))
    password1 = forms.CharField(label="Parola", widget=forms.PasswordInput)
    password2 = forms.CharField(label="Confirmare parola", widget=forms.PasswordInput)

    class Meta:
        model = User
        fields = ['email', 'nume', 'prenume', 'telefon', 'password1', 'password2']

    def save(self, commit=True):
        user = super(UserRegisterForm, self).save(commit=False)
        user.nume = self.cleaned_data['nume']
        user.prenume = self.cleaned_data['prenume']
        user.email = self.cleaned_data['email']
        user.telefon = self.cleaned_data['telefon']
        if commit:
            user.save()
        return user

# removing the suggestion for password complexity
    def __init__(self, *args, **kwargs):
        super(UserCreationForm, self).__init__(*args, **kwargs)

        for fieldname in ['password1', 'password2']:
            self.fields[fieldname].help_text = None


# Update the profile information of the user
class UserUpdateForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ['nume', 'prenume', 'telefon']
        

class UserAdminCreationForm(forms.ModelForm):
    """A form for creating new users from Django Admin"""
    password1 = forms.CharField(label="Parola", widget=forms.PasswordInput)
    password2 = forms.CharField(label="Confirmare parola", widget=forms.PasswordInput)

    class Meta:
        model = User
        fields = ('email', 'nume', 'prenume', 'telefon')

    def clean_password2(self):
        # Check the two password entries match
        password1 = self.cleaned_data.get('password1')
        password2 = self.cleaned_data.get('password2')
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError("Password don't match")
        return password2

    def save (self, commit=True):
        # Save the provided password in hashed format
        user = super(UserAdminCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data['password1'])
        if commit:
            user.save()
        return user


class UserAdminChangeForm(forms.ModelForm):
    """A form for updating users. Includes all the fields on the user,
    but replaces the password field with admin's password hash display
    field"""
    password = ReadOnlyPasswordHashField(label=("Password"),
                                         help_text=("Raw passwords are not stored, so there is no way to see "
                                                    "this user's password, but you can change the password "
                                                    "using <a href=\"../password/\">this form</a>."))

    class Meta:
        model = User
        fields = ('email', 'password', 'nume', 'prenume')

    def clean_password(self):
        return self.initial["password"]