from django.shortcuts import render, redirect
from django.contrib import messages
from .forms import InvoiceRegister, InvoiceRegisterUser
from django.http import HttpResponse
from django.contrib.auth.decorators import login_required
from django.template import loader
from .models import Payment, PaymentUser
from django.views.generic import (DetailView, ListView, UpdateView, DeleteView, CreateView)
from users.models import User
from django import forms

@login_required
def success_request(request):
    return render(request, 'payments/success.html')


@login_required
def payments(request):
    all_info = Payment.objects.filter(email=request.user)
    return render(request, 'payments/payments.html', {'all':all_info})


# Invoice form render for non-users
def invoice_register(request):
    if request.method == 'POST':
        form = InvoiceRegister(request.POST or None)
        if form.is_valid() and not request.user.is_authenticated:
            form.save()
            suma = form.cleaned_data.get('suma')
            numarfact = form.cleaned_data.get('numarfact')
            email = form.cleaned_data.get('email')
            messages.success(request, f'Plata a fost inregistrata cu success pentru factura cu numarul: {numarfact}'
                                    f' in valoare '
                                    f'de: {suma} LEI. Confirmarea platii a fost trimisa la adresa '
                                    f'dumneavoastra: {email}')
            return redirect('/')
    else:
        form = InvoiceRegister()
    return render(request, 'payments/payment.html', {'form':form})


# Invoice form render for authenticated users
@login_required
def invoice_register_user(request):
    if request.method == 'POST':
        form = InvoiceRegisterUser(request.POST or None)
        if form.is_valid() and request.user.is_authenticated:
            form.cleaned_data["email"] = request.user.email
            suma = form.cleaned_data.get('suma')
            numarfact = form.cleaned_data.get('numarfact')
            messages.success(request, f'Plata a fost inregistrata cu success pentru factura cu numarul: {numarfact} '
                                        f' in valoare '
                                        f'de: {suma} LEI. Confirmarea platii a fost trimisa la adresa '
                                        f'dumneavoastra: {request.user.email}')
            form.save()
            return redirect('success')
    else:
        form = InvoiceRegisterUser()
    return render(request, 'payments/payment_user.html', {'form':form, "title":"Bill pay"})


# Register invoice when the user is logged in
@login_required
def invoice_user_register(request):
    return render(request, 'payments/payment_user.html')








