from django.db import models
from django.views.generic.detail import DetailView
from django.contrib.auth.models import (
    AbstractBaseUser, BaseUserManager
)
from users.models import User


# Invoice model for non-users
class Payment(models.Model):
    numarfact = models.IntegerField(blank=False, null=False)
    cumparator = models.CharField(max_length=50, blank=False, null=False)
    suma = models.DecimalField(decimal_places=2, max_digits=20)
    email = models.EmailField(max_length=255, blank=False, null=False)
    telefon = models.CharField(max_length=11, blank=False, null=False)
    clientid = models.IntegerField(blank=True, null=False, unique=False)
    payment_date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return str(self.numarfact)

    def get_cumparator(self):
        return self.cumparator

    def get_suma(self):
        return self.suma

    def get_phone(self):
        return self.telefon

    def get_clientid(self):
        return self.clientid

    def get_email(self):
        return self.email

    def get_payment_date(self):
        return str(self.payment_date)

    def has_perm(self, perm, obj=None):
        return True

    def has_module_perms(self, app_label):
        return True


class PaymentUser(models.Model):
    numarfact = models.IntegerField(blank=False, null=False)
    cumparator = models.CharField(max_length=50, blank=False, null=False)
    suma = models.DecimalField(decimal_places=2, max_digits=20)
    email = models.ForeignKey(User, blank=False, null=False, on_delete=models.CASCADE)
    telefon = models.CharField(max_length=11, blank=False, null=False)
    clientid = models.IntegerField(blank=True, null=False, unique=False)
    payment_date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return str(self.numarfact)

    def get_cumparator(self):
        return self.cumparator

    def get_suma(self):
        return self.suma

    def get_phone(self):
        return self.telefon

    def get_clientid(self):
        return self.clientid

    def get_email(self):
        return self.email

    def get_payment_date(self):
        return str(self.payment_date)

    def has_perm(self, perm, obj=None):
        return True

    def has_module_perms(self, app_label):
        return True




