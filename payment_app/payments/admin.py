from django.contrib import admin
from .models import Payment
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
# Register your models here.


class PaymentProfileAdmin(admin.ModelAdmin):
    list_display = ('numarfact', 'clientid', 'suma', 'cumparator', 'email', 'payment_date')
    list_filter = ('clientid', 'email')
    search_fields = ('email', 'clientid', 'numarfact', 'suma')


admin.site.register(Payment, PaymentProfileAdmin)


