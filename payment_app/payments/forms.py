from django import forms
from .models import Payment
from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _
from users.models import User


def validate_fullname(value):
    val = value.split()
    if len(val) < 2:
        raise ValidationError(
            _('%(value)s nu reprezinta numele complet'),
            params={'value': value},
        )


class InvoiceRegister(forms.ModelForm):
    numarfact = forms.IntegerField(widget=forms.TextInput(attrs={"placeholder":"Numarul facturii"}))
    cumparator = forms.CharField(widget=forms.TextInput(attrs={"placeholder":"Nume si Prenume de pe factura"}),
                                 validators=[validate_fullname])
    clientid = forms.IntegerField(widget=forms.TextInput(attrs={"placeholder":"Id-ul de client de pe factura"}))
    suma = forms.DecimalField(widget=forms.TextInput(attrs={"placeholder":"Suma de plata din factura"}))
    email = forms.EmailField(widget = forms.TextInput(attrs={"placeholder":"Adresa dumneavoastra de email"}))
    telefon = forms.CharField(widget=forms.TextInput(attrs={"placeholder":"Numar de telefon"}))

    class Meta:
        model = Payment
        fields = ['numarfact', 'cumparator', 'clientid', 'suma', 'telefon', 'email']

    def save(self, commit=True):
        invoice = super(InvoiceRegister, self).save(commit=False)
        invoice.numarfact = self.cleaned_data['numarfact']
        invoice.cumparator = self.cleaned_data['cumparator']
        invoice.clientid = self.cleaned_data['clientid']
        invoice.suma = self.cleaned_data['suma']
        invoice.email = self.cleaned_data['email']
        invoice.telefon = self.cleaned_data['telefon']
        if commit:
            invoice.save()
        return invoice

class InvoiceRegisterUser(InvoiceRegister):
    email = forms.CharField(widget = forms.HiddenInput(), required = False)

