# Payment_app description
A web user interface tool to pay monthly bills using the credit card. In the backend the superuser can access users and payments tables with all the required information

# Prerequisites
Need to have installed: 
- Python 3.5 or later and pip
- Mysql server
- Django 3.0 or later

# Installing

Installation via `requirements.txt`:

```
$ git clone https://gitlab.com/Chihaia/payment_app.git
$ cd payment_app
$ python3 -m venv myenv
$ source myenv/bin/activate
$ pip3 install -r requirements.txt
$ python3 manage.py runserver
```

# Usage
The expected output is to have a flexible payment app with authentification system using the email address. The scope is to adapt the code very easy for each business model in the market. Admin page, Models and Forms are easy to modify per customer requirement. 

# Support 
Email - `sorin.chihaia@gmail.com`






